lock "~> 3.10.0"

set :application, "Karma"
set :repo_url, "https://ruslansalikhov@bitbucket.org/karmared/karma-middleware-legacy.git"

set :deploy_to, '/home/api/'
set :branch, 'master'
set :keep_releases, 3

set :format, :pretty
set :log_level, :debug
set :pty, true

set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}
set :linked_files, %w{config/database.yml config/secrets.yml}
set :bundle_binstubs, nil

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :deploy, "deploy:migrate"
  after :publishing, 'deploy:restart', 'sidekiq:restart'
  after :finishing, 'deploy:cleanup'
end
