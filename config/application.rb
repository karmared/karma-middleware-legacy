require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module KarmaBackend
  class Application < Rails::Application
    config.autoload_paths += %w(#{config.root}/app/models/ckeditor)

    config.active_job.queue_adapter = :sidekiq

    #config.action_cable.allowed_request_origins = ['http://localhost:4000', 'https://app.karma.red', '0.0.0.0:4000', 'https://a-tn-front.graphenelab.org']
    config.action_cable.allowed_request_origins = ['*']

    Rails.application.config.middleware.insert_before 0, Rack::Cors do
      allow do
        #origins 'localhost:4000', 'https://app.karma.red', '0.0.0.0:4000', 'https://testnet-app.karma.red', '127.0.0.1:8080', 'https://a-tn-front.graphenelab.org'
        origins '*'
        resource '*',
                 headers: :any,
                 methods: [:get, :post, :put, :patch, :delete, :options, :head],
                 max_age: 0
      end
    end
  end
end
