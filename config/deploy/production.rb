server '138.201.136.41', user: 'api', roles: %w{app db web}
set :ssh_options, forward_agent: true
set :rails_env, :production
set :deploy_to, '/home/api/'
