source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.0.6'

gem 'pg', '~> 0.18'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'carrierwave'
gem 'carrierwave-base64'
gem 'rmagick'
gem 'mini_magick'
gem 'enumerize'
gem 'ckeditor', github: 'galetahub/ckeditor'
gem 'rack-cors', require: 'rack/cors'

gem 'grape'
gem 'grape-active_model_serializers'
gem 'hashie-forbidden_attributes'
gem 'grape-swagger'
gem 'grape-swagger-ui'

gem 'activeadmin'
gem 'devise'
gem 'slim-rails'
gem 'sidekiq'

group :development, :test do
  gem 'byebug', platform: :mri
end

group :development do
  gem 'letter_opener'
  gem 'pry'
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  gem 'capistrano-passenger', git: 'https://github.com/capistrano/passenger.git', branch: 'master'
  gem 'capistrano-faster-assets', '~> 1.0'
  gem 'capistrano-sidekiq', github: 'seuros/capistrano-sidekiq'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
