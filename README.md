# README

## dependencies

* Ruby 2.4.0
* Rails 5.0.5
* PostgreSQL 9.4.10

## db setup
###### development
* run `cp config/database.yml.example  config/database.yml`
* run `rails db:schema:load`
* run `rails db:seed`
* Watch *settings* in the *available rake tasks* section
###### production
* Watch *settings* in the *available rake tasks* section


## secret keys setup
###### development
* run `cp config/secrets.yml.example  config/secrets.yml`
* insert your keys

## mailchimp
  watch `config/secrets.yml.example` for required secrets
