class AddContractTypeIdToContractTemplate < ActiveRecord::Migration[5.0]
  def change
    add_column :contract_templates, :contract_type_id, :integer
    remove_column :contract_templates, :contract_type, :string
  end
end
