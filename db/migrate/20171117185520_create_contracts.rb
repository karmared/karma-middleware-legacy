class CreateContracts < ActiveRecord::Migration[5.0]
  def change
    create_table :contracts do |t|
      t.text :body
      t.integer :user_id
      t.integer :contract_template_id
      t.jsonb :data

      t.timestamps
    end
  end
end
