class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :confirmed, :boolean
    add_column :users, :confirm_key, :string
  end
end
