class CreateContractTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :contract_templates do |t|
      t.text :body
      t.string :contract_type
      t.text :variables

      t.timestamps
    end
  end
end
