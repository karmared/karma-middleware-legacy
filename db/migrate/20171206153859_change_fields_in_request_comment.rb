class ChangeFieldsInRequestComment < ActiveRecord::Migration[5.0]
  def change
    rename_column :request_comments, :user_id, :sender_id
    add_column :request_comments, :receiver_id, :integer
  end
end
