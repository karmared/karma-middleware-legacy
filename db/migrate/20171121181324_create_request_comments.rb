class CreateRequestComments < ActiveRecord::Migration[5.0]
  def change
    create_table :request_comments do |t|
      t.text :body
      t.integer :user_id

      t.timestamps
    end
  end
end
