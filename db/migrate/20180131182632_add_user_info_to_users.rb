class AddUserInfoToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :full_name, :string
    add_column :users, :phone, :string
    add_column :users, :bank_account, :string
    add_column :users, :about, :text
  end
end
