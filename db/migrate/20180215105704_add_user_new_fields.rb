class AddUserNewFields < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :full_name, :string
    remove_column :users, :about, :string
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :tax_residence, :string
    add_column :users, :about_me, :text
    add_column :users, :bank_name, :string
    add_column :users, :bank_swift, :string
    add_column :users, :bank_name_of_beneficiary, :string
    add_column :users, :company_name, :string
    add_column :users, :company_activity, :string
    add_column :users, :company_vat_id, :string
    add_column :users, :company_website, :string
    add_column :users, :company_pdf_presentation, :string
  end
end
