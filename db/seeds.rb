AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') 

StaticPage.page_type.values.each do |page_type|
  StaticPage.create(title: page_type.capitalize, page_type: page_type)
end
