# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180215105704) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type", using: :btree
  end

  create_table "contract_templates", force: :cascade do |t|
    t.text     "body"
    t.text     "variables"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "contract_type_id"
  end

  create_table "contract_types", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contracts", force: :cascade do |t|
    t.text     "body"
    t.integer  "user_id"
    t.integer  "contract_template_id"
    t.jsonb    "data"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "request_comments", force: :cascade do |t|
    t.text     "body"
    t.integer  "sender_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "receiver_id"
  end

  create_table "static_pages", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.string   "page_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "passport_number"
    t.string   "avatar"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "username"
    t.boolean  "confirmed"
    t.string   "confirm_key"
    t.string   "phone"
    t.string   "bank_account"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "tax_residence"
    t.text     "about_me"
    t.string   "bank_name"
    t.string   "bank_swift"
    t.string   "bank_name_of_beneficiary"
    t.string   "company_name"
    t.string   "company_activity"
    t.string   "company_vat_id"
    t.string   "company_website"
    t.string   "company_pdf_presentation"
  end

end
