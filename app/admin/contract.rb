ActiveAdmin.register Contract do
  menu parent: 'Contracts'
  
  actions :index, :show

  show do
    attributes_table do
      row :user
      row :contract_template
      row :created_at
      row :body do |contract|
        raw(contract.body)
      end
    end
  end

  index do
    selectable_column
    id_column
    column :body do |contract|
      raw(contract&.body&.first(200))
    end
    column :user
    column :contract_template
    column :created_at
    actions
  end
end
