ActiveAdmin.register RequestComment do
  permit_params :body, :sender_id, :receiver_id 

  index do
    selectable_column
    id_column
    column :sender
    column :receiver
    column :created_at
    actions
  end
end
