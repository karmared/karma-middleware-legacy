ActiveAdmin.register User do
  permit_params :username,
                :first_name,
                :last_name,
                :email,
                :phone,
                :tax_residence,
                :passport_number,
                :about_me,
                :avatar,
                :bank_name,
                :bank_swift,
                :bank_account,
                :bank_name_of_beneficiary,
                :company_name,
                :company_activity,
                :company_vat_id,
                :company_website,
                :company_pdf_presentation


  index do
    selectable_column
    id_column
    column :email
    column :username
    column :first_name
    column :last_name
    column :tax_residence
    column :passport_number
    column :phone
    column :about_me
    column :bank_name
    column :bank_swift
    column :bank_account
    column :bank_name_of_beneficiary
    column :company_name
    column :company_activity
    column :company_vat_id
    column :company_website
    column :company_pdf_presentation
    column :avatar do |user|
      user.avatar.present? ? image_tag(user.avatar.url(:thumb)) : content_tag(:span, "no avatar")
    end   
    actions 
  end

  form do |f|
    inputs 'User' do
      input :email
      input :username
      input :first_name
      input :last_name
      input :passport_number
      input :phone
      input :tax_residence
      input :about_me
      input :avatar, as: :file, hint: f.object.avatar.present? ? image_tag(f.object.avatar.url(:thumb)) : content_tag(:span, "no avatar")
      input :avatar_cache, as: :hidden
      input :bank_name
      input :bank_swift
      input :bank_account
      input :bank_name_of_beneficiary
      input :company_name
      input :company_activity
      input :company_vat_id
      input :company_website
      input :company_pdf_presentation
    end
    actions
  end
end
