ActiveAdmin.register ContractTemplate do
  menu parent: 'Contracts'

  permit_params :body, :variables, :contract_type_id

  index do
    selectable_column
    column :contract_type
    actions
  end

  form do |f|
    inputs 'Contract Template' do 
      input :contract_type, as: :select, collection: ContractType.all.map{ |u| [u.title, u.id] }
      input :body, as: :ckeditor, hint: 'For using variables, use the construction: variable_name'
      input :variables
    end
    actions
  end
end
