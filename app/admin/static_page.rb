ActiveAdmin.register StaticPage do
  actions :index, :show, :edit, :update

  permit_params :title, :body

  index do
    selectable_column
    column :title
    column :page_type
    actions
  end

  form do |f|
    inputs 'Static Page' do 
      input :title
      input :page_type, input_html: { disabled: true }
      input :body, as: :ckeditor
    end
    actions
  end
end
