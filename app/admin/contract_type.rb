ActiveAdmin.register ContractType do
  menu parent: 'Contracts'
  
  permit_params :title

  form do |f|
    inputs 'Contract Type' do 
      input :title
    end
    actions
  end
end
