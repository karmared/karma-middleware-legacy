class UserMailer < ApplicationMailer
  def request_comment(receiver, sender, comment)
    @receiver = receiver
    @sender = sender
    @comment = comment

    mail to: "#{@receiver.username} <#{@receiver.email}>", subject: "#{@sender.username} commented your query"
  end

  def confirm_email(user, confirm_key)
    @user = user
    @confirm_key = confirm_key

    mail to: "#{@user.username} <#{@user.email}>", subject: 'Email Confirmation'
  end
end
