class ApplicationMailer < ActionMailer::Base
  default from: 'Karma Trust <no-reply@karma.red>'

  layout 'mailer'
end
