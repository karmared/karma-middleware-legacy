class User < ApplicationRecord
  has_many :contracts, dependent: :nullify
  has_many :request_comments, dependent: :nullify

  validates :username, presence: true

  mount_base64_uploader :avatar, ImageUploader

  after_update :send_confirmation_mail, if: -> { self.email_changed? }
  after_create :send_confirmation_mail, if: -> { self.email.present? }

  def send_confirmation_mail
    self.confirm_key = SecureRandom.hex
    self.confirmed = false

    ConfirmEmailJob.perform_later(self.id, self.confirm_key)
  end
end
