class ContractType < ApplicationRecord
  has_one :contract_template

  validates :title, uniqueness: true
end
