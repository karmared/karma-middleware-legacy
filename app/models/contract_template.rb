class ContractTemplate < ApplicationRecord
  has_many :contracts, dependent: :nullify
  belongs_to :contract_type
end
