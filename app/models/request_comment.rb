class RequestComment < ApplicationRecord
  belongs_to :sender, foreign_key: :sender_id, class_name: 'User'
  belongs_to :receiver, foreign_key: :receiver_id, class_name: 'User'

  after_create :send_comment_mail

  def send_comment_mail
    SendRequestCommentJob.perform_later(self.receiver_id, self.sender_id, self.body)
  end
end
