class StaticPage < ApplicationRecord
  extend Enumerize

  enumerize :page_type, in: [:terms]
end
