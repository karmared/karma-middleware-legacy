class Contract < ApplicationRecord
  belongs_to :user
  belongs_to :contract_template

  before_create :update_body

  protected

  def update_body
    data.each do |key, value|
      self.body.gsub!(key, value)
    end   
  end
end
