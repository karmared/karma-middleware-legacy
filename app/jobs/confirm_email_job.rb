class ConfirmEmailJob < ApplicationJob
  queue_as :default

  def perform(user_id, confirm_key)
    user = User.find(user_id)

    UserMailer.confirm_email(user, confirm_key).deliver_now
  end
end
