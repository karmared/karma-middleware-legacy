class SendRequestCommentJob < ApplicationJob
  queue_as :default

  def perform(receiver_id, sender_id, comment)
    receiver = User.find(receiver_id)
    sender = User.find(sender_id)

    UserMailer.request_comment(receiver, sender, comment).deliver_now
  end
end
