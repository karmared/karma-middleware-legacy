class StaticPageSerializer < ActiveModel::Serializer
  attributes :title, :body
end
