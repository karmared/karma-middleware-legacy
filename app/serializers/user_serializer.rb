class UserSerializer < ActiveModel::Serializer
  attributes :id,
             :username,
             :first_name,
             :last_name,

             :phone,
             :passport_number,
             :tax_residence,
             :about_me,
             :avatar,

             :bank_name,
             :bank_swift,
             :bank_account,
             :bank_name_of_beneficiary,

             :company_name,
             :company_activity,
             :company_vat_id,
             :company_website,
             :company_pdf_presentation,

             :email,
             :confirmed
end
