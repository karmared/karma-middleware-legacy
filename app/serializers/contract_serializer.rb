class ContractSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :body
end
