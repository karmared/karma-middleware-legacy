module Karma
  class Base < Grape::API
    format :json
    formatter :json, Grape::Formatter::ActiveModelSerializers

    prefix :api
    
    mount Karma::V1
  end
end
