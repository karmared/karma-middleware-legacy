module Karma
  class V1::StaticPages < Grape::API
    
    resources :static_pages do

      route_param :page_type do
        desc 'Get static_page by slug'
        params do
          requires :page_type, values: StaticPage.page_type.values
        end
        get do
          static_page = StaticPage.find_by(page_type: params[:page_type])
          static_page
        end
      end

    end

  end
end
