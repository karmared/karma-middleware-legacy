module Karma
  class V1::Contracts < Grape::API
    
    resources :contracts do
      
      desc 'New contract'
      params do
        requires :username
        #requires :contract_type, values: ContractType.all.map(&:title)
        optional :data
      end
      post do
        user = User.find_by(username: params[:username])
        contract_type = ContractType.find_by(title: params[:contract_type]).take
        if contract_type.present?
          contract_template = contract_type.contract_template
          contract = Contract.create(body: contract_template.body, contract_template_id: contract_template.id, data: JSON.parse(params[:data]), user_id: user.id)
        end
      end
    end

  end
end
