module Karma
  class V1::RequestComments < Grape::API
    
    resources :request_comments do
      
      desc 'New comment'
      params do
        requires :sender
        requires :receiver
        requires :body
      end
      post do
        sender = User.find_by(username: params[:sender])
        receiver = User.find_by(username: params[:receiver])
        RequestComment.create(body: params[:body], sender_id: sender.id, receiver_id: receiver.id) if sender && receiver
      end
    end

  end
end
