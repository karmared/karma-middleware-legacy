module Karma
  class V1::Users < Grape::API
    
    resources :users do
      desc 'Authorization'
      params do
        requires :username, type: String
        # optional :token, type: String
      end
      post :auth do
        User.find_or_create_by(username: params[:username])
      end
      
      route_param :username do
        before do
          @user = User.find_by(username: params[:username])
        end
        
        desc 'Update user info'
        params do
          optional :email, type: String
          optional :username, type: String
          optional :first_name, type: String
          optional :last_name, type: String
          optional :passport_number, type: String
          optional :tax_residence, type: String
          optional :avatar, type: String
        end
        patch do
          @user.update_attributes(params)
          @user
        end

        desc 'Get user info'
        get do
          @user
        end

        desc 'Send confirmation link'
        get 'send_confirmation' do
          @user.confirm_key = SecureRandom.hex
          @user.confirmed = false
          @user.save

          ConfirmEmailJob.perform_later(@user.id, @user.confirm_key)
          @user
        end

        route_param :confirm_key do
          desc 'Confirm email'
          post do
            @user.update_attributes(confirmed: true) if @user.confirm_key == params[:confirm_key]
            @user
          end
        end
      end
    end

  end
end
