module Karma
  class V1 < Grape::API
    version 'v1', using: :path, vendor: 'projects_for_good'
    content_type :json, 'application/json; charset=utf-8'

    rescue_from ActiveRecord::RecordInvalid do |error|
      build_error!(error.record.errors, 422)
    end

    rescue_from ActiveRecord::RecordNotFound do |error|
      build_error!('Record not found', 404)
    end

    mount Karma::V1::Users
    mount Karma::V1::Contracts
    mount Karma::V1::RequestComments
    mount Karma::V1::StaticPages

    add_swagger_documentation api_version: 'v1', 
                              base_path: '/api',
                              info: {
                                title: "Karma API"
                              }
  end
end
